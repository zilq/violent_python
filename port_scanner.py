#!/usr/bin/env python3


import optparse
import output_printer as printer
from output_printer import OutputType
import re
import util
from parser_config import ParserConfig, ParserOptionItem

PARSER_USAGE = "port_scanner -H <hostname> -p <ports>"
PARSER_OPTIONS = [
    ParserOptionItem("-H", "hostname", "string", "specify hostname"),
    ParserOptionItem("-p", "ports", "string", "specify ports")
]
PARSER_CONFIG = ParserConfig(PARSER_USAGE, PARSER_OPTIONS)


def main():
    (hostname, ports) = get_arguments()


def get_arguments():
    parser = util.get_parser(PARSER_CONFIG)
    (options, args) = parser.parse_args()
    if (options.hostname == None) or (options.ports == None):
        printer.print("usage: {0}", OutputType.VALUE, [parser.usage])
        util.quit("Exiting", 0)
    else:
        return (options.hostname, parse_ports(options.ports))


def parse_ports(port_string: str) -> list:
    split_ports: list = port_string.split(',')
    ports: list = []
    for item in split_ports:
        validate_port_string(item)
        split_port_item: list = [int(port) for port in item.split('-')]
        validate_ports(split_port_item)
        if (len(split_port_item) > 1):
            ports.extend(get_ports_in_range(split_port_item))
        else:
            ports.extend(split_port_item)

    ports.sort()
    return ports


def validate_port_string(port_string: str):
    if (not is_valid_port_string(port_string)):
        print_invalid_port_argument_error()
        util.quit("Exiting", -1)


def is_valid_port_string(port) -> bool:
    return re.search("^\d+-\d+$|^\d+$", port) is not None


def print_invalid_port_argument_error():
    printer.print("Invalid port argument. " + \
        "Enter comma separated ports or port ranges, e.g. {0}", \
            OutputType.VALUE, ["80-443,555,666"])


def validate_ports(ports: list):
    if (not all([is_valid_port_number(port) for port in ports])):
        print_invalid_port_number_error()
        util.quit("Exiting", -1)


def is_valid_port_number(port: int) -> bool:
    return port >= 0 and port <= 65535


def print_invalid_port_number_error():
    printer.print("Invalid port number. Valid values are between {0}", \
        OutputType.VALUE, ["0 - 65535"])


def get_ports_in_range(ports: list) -> list:
    ports.sort()
    ports[len(ports)-1] += 1
    return list(range(*ports))


if __name__ == "__main__":
    main()