class ParserConfig:

    def __init__(self, usage: str, options: list):
        self.usage = usage
        self.options = options


class ParserOptionItem:

    def __init__(self, flag: str, dest: str, type: str, help: str):
        self.flag = flag
        self.dest = dest
        self.type = type
        self.help = help

