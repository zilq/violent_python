from unix_pass_cracker import (
    get_user_password_hashes_by_username,
    get_password_candidates,
    find_matching_password
)


def test_get_password_hashes_from_file_succeeds():
    password_hashes_by_username: dict = get_user_password_hashes_by_username()
    assert len(password_hashes_by_username.keys()) == 2


def test_get_password_candidates_succeeds():
    password_candidates: list = get_password_candidates()
    assert len(password_candidates) == 4


def test_find_matching_passwords_finds_a_match():
    match: str = find_matching_password("HX9LLTdc/jiDE", ["butter", "milk", "egg"])
    assert match == "egg"


def test_find_matching_passwords_finds_no_match():
    match: str = find_matching_password("HX9LLTdc/jiDE", ["butter", "milk"])
    assert match == None