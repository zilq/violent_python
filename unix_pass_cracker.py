#!/usr/bin/env python3

import crypt
import logging
import output_printer as printer
from output_printer import OutputType
import file_utils


def main():
    password_hashes_by_username: dict = get_user_password_hashes_by_username()
    password_candidates: list = get_password_candidates()
    passwords_by_username: dict = get_passwords_by_username(
        password_hashes_by_username,
        password_candidates
    )
    print_user_passwords(passwords_by_username)


def get_user_password_hashes_by_username() -> dict:
    file_contents: list = file_utils.get_file_contents("hashes")

    hashes_by_username: dict = {}
    for item in file_contents:
        username_and_pass_hash: tuple = get_user_and_hash(item)
        username: str = username_and_pass_hash[0]
        hash: str = username_and_pass_hash[1]
        hashes_by_username[username] = hash
    
    return hashes_by_username


def get_user_and_hash(item: str) -> tuple:
    parts: list = item.split(':')
    return (parts[0].strip(), parts[1].strip())


def get_password_candidates() -> list:
    return file_utils.get_file_contents("password_candidates")


def get_passwords_by_username(hashes_by_username: dict,
                              password_candidates: list) -> dict:
    logging.info("Finding possible password candidates")
    printer.print("Finding possible password candidates", OutputType.INFO)

    found_passwords_by_user: dict = {}
    for user in hashes_by_username.keys():
        discovered_password: str = find_matching_password(
            hashes_by_username.get(user),
            password_candidates
        )

        if discovered_password is not None:
            found_passwords_by_user[user] = discovered_password

    return found_passwords_by_user


def find_matching_password(password_hash: str,
                           password_candidates: list) -> str:
    salt: str = password_hash[0:2]
    for candidate in password_candidates:
        candidate_hash: str = crypt.crypt(candidate, salt)
        if candidate_hash == password_hash:
            return candidate

    return None


def print_user_passwords(passwords_by_username: dict):
    if (len(passwords_by_username.keys()) == 0):
        printer.print("No passwords found", OutputType.FAIL)
        return
    
    printer.print("Found passwords for the following users:", OutputType.SUCCESS)
    for user in passwords_by_username.keys():
        printer.print(f'{user}: {{0}}', OutputType.VALUE, [passwords_by_username.get(user)])


if __name__ == "__main__":
    main()