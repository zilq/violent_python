from parser_config import ParserConfig
import optparse
import output_printer as printer
from output_printer import OutputType


def quit(message: str, code: int):
    printer.print(message, OutputType.FAIL if code < 0 else OutputType.SUCCESS)
    exit(code)


def get_parser(config: ParserConfig) -> optparse.OptionParser:
    parser = optparse.OptionParser(config.usage)
    for option in config.options:
        parser.add_option(option.flag, dest=option.dest, type=option.type, help=option.help)

    return parser